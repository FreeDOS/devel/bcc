


ld86(1)                                                   ld86(1)


NAME
     ld86 - Linker for as86(1)

SYNOPSIS
     ld86   [-03MNdimrstyz[-]]   [-llib_extension]   [-o outfile]
     [-Ccrtfile] [-Llibdir] [-Olibfile] [-Ttextaddr] [-Hheapsize]
     [-Ddataaddr] infile...

DESCRIPTION
     This  linker  understands  only the object files produced by
     the as86 assembler, it can link them into either  an  impure
     or a separate I&D executable.

     The  linking  defaults are everything off or none except for
     -0 and the output file is a.out.  There is  not  a  standard
     library location defined in the linker.

OPTIONS

     -0   produce header with 16-bit magic

     -3   produce header with 32-bit magic

     -d   delete  the header from the output file, used for MSDOS
          COM files.  As a side effect this also includes  -s  as
          there's nowhere to put a symbol table.

     -Cx  add  file  libdir-from-search/crtx.o  to  list of files
          linked

     -D   data base  address  follows  (in  format  suitable  for
          strtoul)

     -H   the  top  of  heap  (initial  stack) address (in format
          suitable for strtoul)

     -Lx  add dir name x to the head of the list of library  dirs
          searched

     -M   print symbols linked on stdout

     -N   Create  a  native  Linux  OMAGIC  output  file.  If the
          contents are i386 code the binary can be either  linked
          by  GCC or executed by linux.  If the -z option is also
          included the linker can generate a QMAGIC executable.

     -Ox  add library or object file libdir-from-search/x to list
          of files linked

     -T   text  base  address  follows  (in  format  suitable for
          strtoul)

     -i   separate I&D output




                            Apr, 1997                           1





ld86(1)                                                   ld86(1)


     -lx  add library libdir-from-search/libx.a to list of  files
          linked

     -m   print modules linked on stdout

     -o   output file name follows

     -s   strip symbols

     -r   Generate  a  relocatable object from one source object,
          if the linker is given the -N option  also  the  output
          format will be the hosts native format if possible.

     -t   trace modules being looked at on stdout

     -y   Alter  the  symbol  tables to add label 'extensions' so
          that labels with more than 8 characters can  be  stored
          in elks executables.

     -z   produce "unmapped zero page" or "QMAGIC" executables

     All  the options not taking an argument may be turned off by
     following the option letter by a '-', as for cc1.

PREDEFINED LABELS
     The  linker  predefines  several labels that can be imported
     into user programs.

     __etext
          Standard C variable for the end of the text segment.

     __edata
          Standard C variable for the end of the initilised data.

     __end
          Standard C variable for the end of the bss area.

     __segoff
          The offset within the executable file between the start
          of  the  text segment and the start of the data segment
          in 16 byte 'paragraphs'.  Note this is zero for  impure
          (tiny   model)   executables   and   is   adjusted  for
          executables that don't start at  offset  0  within  the
          segment.

     __segXDL
          The lowest address  with  data  in  segment  'X'.   (eg
          __seg0DL  is  for  segment  zero  or  the text segment,
          __seg3DL is for the data segment) The value  'X'  is  a
          hex digit.

     __segXDH
          The top of segment 'X's data area.




                            Apr, 1997                           2





ld86(1)                                                   ld86(1)


     __segXCL
          The  bottom of segment 'X's 'common data' or unitilised
          data area.  Each segment has  both  an  initilised  and
          unitilised data area.

     __segXCH
          The top of segment 'X's common area.

     __segXSO
          This is the adjusted offset from segment 0 of the start
          of segment 'X' in 'paragraphs'.

HISTORY

     The 6809 version does not support -i.

     The previous versions of the linker could  produce  an  8086
     executable  with  segments  of  a  size  >64k, now only i386
     executables may have segments this large.

BUGS
     The  linker  cannot  deal  with  reverse seeks caused by org
     instructions in the object file.  Unlike  previous  versions
     the  current  one traps the error rather than trying to fill
     up the hard disk.

     The linker produces a broken a.out object file if given  one
     input  and  the  -r  option this is so it is compatible with
     pre-dev86 versions.




























                            Apr, 1997                           3


